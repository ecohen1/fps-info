i added these two scripts to my bin to make it easier to use them

ffsplit.sh is a script i found online to split an mp4 into equal sized chunks

usage: ffsplit.sh <filename>.mp4 <length of windows in seconds>
e.g. ffsplit.sh vid.mp4 10 will create 6 files if vid.mp4 is 60s long


analyze_chunks.sh is a script i wrote to parse this data. 

usage: analyze_chunks.sh

the way i use these scripts is i have vids/, output/, and result/ dirs. i put the video in a subfolder of vids/, run ffsplit on it, then run analyze chunks (no args required), and text files with the framerates will appear in a text file in result/

note: script is very dependent on filenames, make sure you do not run analyze_chunks twice on a file with the same name or some data will be overwritten
