max=$(ls -1 | wc -l)
basefile=$(ls | head -n 1)

for i in $(seq 1 $(echo $max-1 | bc))
do
    numstr=000$i
    numstr2=${numstr:(-3)}
    filename=$basefile-$numstr2.$basefile
    
    ffmpeg -i $filename 2>> /Users/eli/output/$basefile.txt
done

grep -o "[0-9][0-9].\?[0-9]\?[0-9]\? fps" /Users/eli/output/$basefile.txt | grep -o "[0-9][0-9].\?[0-9]\?[0-9]\?" > /Users/eli/results/$basefile.txt
